Task Description

Kindly find the image "preview.png"

This is a login form prototype for a responsive website with three types of users,
1- The three DIVs with different colors should be draggable in all the four directions (right – left – up – Down).
2- Consider showing the following points in your task files
   a. code style.
   b. Comments.
   c. Dealing with js libraries (if you choose any of them).
   d. Additionally: Validating the form controls.
$(document).ready(function() {
  $('.draggable').draggable({ 
    // containment: ".usersFormsWrapper",
    scroll: false
  });

  $(window).bind("load resize", function(){
    _wrapHeight = $('.submitSignin').parents('.form-signin-content').height();
    // Setting Height
    $('.submitSignin').css({ 'height':_wrapHeight });

    _firstTypeFormOffset = $('.form-signin.firstTypeForm').offset().top;
    // $('.secondTypeForm, .thirdTypeForm').offset().top(_firstTypeFormOffset);
  });
});